package com.example.step12

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.webkit.WebResourceRequest
import androidx.fragment.app.Fragment
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.annotation.RequiresApi
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView



class WebFragment : Fragment() {

    val urlList = arrayListOf<String?>()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_web, container, false)
        view.findViewById<BottomNavigationView>(R.id.bNavWeb).selectedItemId = R.id.webLabel
        view.findViewById<BottomNavigationView>(R.id.bNavWeb).setOnNavigationItemSelectedListener {
            when (it.itemId){
                R.id.webLabel -> { }

                R.id.logLabel -> {
                    Navigation.findNavController(view).navigate(R.id.fromWebToLogin)
                }

                R.id.homeLabel->{
                    Navigation.findNavController(view).navigate(R.id.fromWebToHome)
                }
            }
            true
        }
        val wb = view.findViewById<WebView>(R.id.wbV)

        val callback = object : OnBackPressedCallback(true){
            override fun handleOnBackPressed() {
                if (wb.canGoBack()){
                    wb.goBack()
                }
                else Navigation.findNavController(view).navigate(R.id.fromWebToHome)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,callback)

        webViewSetup(wb)
        return view
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun webViewSetup(wb:WebView){
        wb.webViewClient= WebViewClient()
        wb.apply {
            loadUrl("https://www.google.com/")
            settings.safeBrowsingEnabled = true
        }

    }

    companion object {
        fun newInstance() = WebFragment()
    }
}