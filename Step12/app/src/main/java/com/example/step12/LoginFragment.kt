package com.example.step12

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.Navigation
import com.google.android.material.bottomnavigation.BottomNavigationView

var dataBase= mutableMapOf<String,String>("admin" to "123456")

const val sign=true

class LoginFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        view.findViewById<BottomNavigationView>(R.id.bNavLog).isVisible=false
        view.findViewById<Button>(R.id.btReg).setOnClickListener{
            if ((view.findViewById<EditText>(R.id.etLoginLogin).text.toString()) in dataBase && view.findViewById<EditText>(R.id.etPasswordLogin).text.toString()
                == dataBase[view.findViewById<EditText>(R.id.etLoginLogin).text.toString()] || sign) {
                Navigation.findNavController(view).navigate(R.id.toHomeFragment)
            }
            else
            {
                Toast.makeText(view.context,"Incorrect login or password",Toast.LENGTH_SHORT).show()
            }
        }
        view.findViewById<TextView>(R.id.tvReg).setOnClickListener { Navigation.findNavController(view).navigate(R.id.toRegistrationFragment) }
        return view
    }

    companion object {

        fun newInstance()=LoginFragment()
    }
}