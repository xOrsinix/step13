package com.example.step12

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.step12.databinding.StringItemBinding
import kotlin.random.Random

class StringAdapter:RecyclerView.Adapter<StringAdapter.StringHolder>() {
    val stringList = ArrayList<String>()
    class StringHolder(item: View):RecyclerView.ViewHolder(item) {
        val binding = StringItemBinding.bind(item)
        @RequiresApi(Build.VERSION_CODES.M)
        fun bind(str:String){
            binding.tvCard.text=str
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.string_item,parent,false)
        return StringHolder(view)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: StringHolder, position: Int) {
        when (Random.nextInt(1,4)){
            1 -> holder.binding.tvCard.setTextAppearance(R.style.myString1)
            2 -> holder.binding.tvCard.setTextAppearance(R.style.myString2)
            3 -> holder.binding.tvCard.setTextAppearance(R.style.myString3)
        }
        holder.bind(stringList[position])
    }

    override fun getItemCount(): Int {
        return stringList.size
    }

    fun addString(str:String){
        stringList.add(str)
        notifyDataSetChanged()
    }

    fun addAll(strList:ArrayList<String>){
        stringList.addAll(strList)
        notifyDataSetChanged()
    }
}